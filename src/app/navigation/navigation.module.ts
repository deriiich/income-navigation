import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationComponent } from './navigation.component';
import { AngularMaterialModule } from '../angular-material.module';



@NgModule({
  declarations: [NavigationComponent],
  imports: [
    CommonModule,
    AngularMaterialModule
  ],
  exports: [NavigationComponent],
})
export class NavigationModule { }
