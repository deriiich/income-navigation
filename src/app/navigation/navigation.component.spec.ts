import { Location } from '@angular/common';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AngularMaterialModule } from '../angular-material.module';
import { routes } from '../app-routing.module';
import { NavigationComponent } from './navigation.component';


describe('NavigationComponent', () => {
    let component: NavigationComponent;
    let fixture: ComponentFixture<NavigationComponent>;
    let router: Router;
    let location: Location;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [RouterTestingModule.withRoutes(routes), AngularMaterialModule,
                BrowserAnimationsModule],
            declarations: [NavigationComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        location = TestBed.get(Location);
        fixture = TestBed.createComponent(NavigationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should get list of pages', () => {
        spyOn(component, 'getListOfPages').and.callFake(() => [{ url: '/dashboard', title: 'Dashboard', icon: 'dashboard' }])

        component.getListOfPages();

        expect(component.getListOfPages).toHaveBeenCalledOnceWith();
        expect(component.getListOfPages()).toEqual([{ url: '/dashboard', title: 'Dashboard', icon: 'dashboard' }]);
    })

});
