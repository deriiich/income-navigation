import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  pages: any[] = [];
  selectedIndex = 0;

  constructor(private router: Router) {
    this.router.events.pipe(filter((event) => event instanceof NavigationEnd)).subscribe((router: any) => this.selectedIndex = this.pages.findIndex(nav => nav.url === router.urlAfterRedirects));
  }

  ngOnInit(): void {
    this.pages = this.getListOfPages();
  }

  getListOfPages() {
    return [
      { url: '/dashboard', title: 'Dashboard', icon: 'dashboard' },
      { url: '/relationship-income', title: 'Relationship Income', icon: 'money' },
      { url: '/deals', title: 'Deals', icon: 'sell' },
      { url: '/access', title: 'Access', icon: 'key' },
      { url: '/details', title: 'Details', icon: 'edit' }
    ];
  }

  onTabChanged(event: any) {
    console.log(event);
    this.router.navigateByUrl(this.pages[event.index].url);
  }



}
