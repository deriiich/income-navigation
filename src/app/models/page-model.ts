export interface PageModel {
    header: string;
    value: number;
}
