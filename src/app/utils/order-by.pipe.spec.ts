import { OrderByPipe } from './order-by.pipe';

describe('OrderByPipe', () => {
  const pipe = new OrderByPipe();

  it('create an instance', () => {
    const pipe = new OrderByPipe();
    expect(pipe).toBeTruthy();
  });

  it('should sort alphabetically', () => {
    const value = [{ name: 'b' }, { name: 'z' }, { name: 'a' }, { name: 'x' }];

    expect(pipe.transform(value, 'name')).toEqual([{ name: 'a' }, { name: 'b' }, { name: 'x' }, { name: 'z' }]);

  })
});
