import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppEntityFilterComponent } from './app-entity-filter.component';
import { AngularMaterialModule } from '../angular-material.module';



@NgModule({
  declarations: [AppEntityFilterComponent],
  imports: [
    CommonModule,
    AngularMaterialModule
  ],
  exports: [AppEntityFilterComponent]
})

export class AppEntityFilterModule { }
