import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-entity-filter',
  templateUrl: './app-entity-filter.component.html',
  styleUrls: ['./app-entity-filter.component.scss']
})
export class AppEntityFilterComponent implements OnInit {
  @Input('relationship') relationship: any;
  @Output('entityChange') entityChange: EventEmitter<any> = new EventEmitter();

  selectedButton: any;

  constructor() { }

  ngOnInit(): void {
  }

  makeButtonActive(object: any) {

    let element: any = document.getElementById(object.name);

    if (!this.selectedButton) {
      this.selectedButton = element;
      this.selectedButton.classList.add('active');
    } else {
      this.selectedButton.classList.remove('active');
    }

    if (element && element.id.includes('Client')) {
      this.selectedButton = element;
      this.selectedButton.classList.add('active');
    }

    this.emitObject(object);
  }


  emitObject(object: any) {
    let newObject = object.children ? { id: object.id, name: object.name } : object

    this.entityChange.emit(newObject);
  }


}
