import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import MockData from '../../assets/pagemodel.json';
import { AppEntityFilterComponent } from './app-entity-filter.component';

describe('AppEntityFilterComponent', () => {
  let component: AppEntityFilterComponent;
  let fixture: ComponentFixture<AppEntityFilterComponent>;
  const mockData = MockData;
  const object = { id: 12345, name: 'test' }
  let buttonElement: any;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppEntityFilterComponent]
    })
      .compileComponents();
    fixture = TestBed.createComponent(AppEntityFilterComponent);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppEntityFilterComponent);
    component = fixture.componentInstance;
    component.relationship = mockData;
    fixture.detectChanges();
    buttonElement = fixture.debugElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call makeButtonActive', fakeAsync(() => {
    spyOn(component, 'makeButtonActive');

    let button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    tick();

    expect(component.makeButtonActive).toHaveBeenCalled();


  }));

  it('should call the emitObject function ', fakeAsync(() => {
    spyOn(component, 'emitObject').and.callThrough();

    let button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    component.makeButtonActive(object);
    tick();


    expect(component.emitObject).toHaveBeenCalled();
    expect(component.emitObject).toHaveBeenCalledWith(object);

  }));

  it('should make the button active ', fakeAsync(() => {
    let element = fixture.debugElement.nativeElement.querySelector('button')
    element.click();
    tick(50);

    expect(component.selectedButton).toBe(element);
    expect(component.selectedButton).toHaveClass('active');

  }));
});
