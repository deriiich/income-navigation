import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-not-ready',
  templateUrl: './not-ready.component.html',
  styleUrls: ['./not-ready.component.scss']
})
export class NotReadyComponent implements OnInit, OnDestroy {

  navigate: any;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.navigateTo();
  }

  navigateTo() {
    this.navigate = setTimeout(() => {
      this.router.navigateByUrl('');
    }, 2000);
  }

  ngOnDestroy(): void {
    clearTimeout(this.navigate);
  }
}
