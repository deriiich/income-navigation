
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NotReadyComponent } from './not-ready.component';

describe('NotReadyComponent', () => {
  let component: NotReadyComponent;
  let fixture: ComponentFixture<NotReadyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [NotReadyComponent],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotReadyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('it should navigate to Relationship Income', () => {
    spyOn(component, 'navigateTo');

    component.ngOnInit();

    expect(component.navigateTo).toHaveBeenCalled();

  });
});
