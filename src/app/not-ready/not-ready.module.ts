import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotReadyRoutingModule } from './not-ready-routing.module';
import { NotReadyComponent } from './not-ready.component';

@NgModule({
  declarations: [NotReadyComponent],
  imports: [
    CommonModule,
    NotReadyRoutingModule,
  ],
})
export class NotReadyModule { }
