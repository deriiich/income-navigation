import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotReadyComponent } from './not-ready.component';

const routes: Routes = [
  {
    path: '',
    component: NotReadyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotReadyRoutingModule { }
