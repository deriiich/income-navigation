import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';
import { AngularMaterialModule } from '../angular-material.module';
import { AppEntityFilterComponent } from '../app-entity-filter/app-entity-filter.component';
import { AppEntityFilterModule } from '../app-entity-filter/app-entity-filter.module';
import { CustomerService } from '../services/customer.service';
import { OrderByPipe } from '../utils/order-by.pipe';

import { RelationshipIncomeComponent } from './relationship-income.component';
import PageModelData from '../../assets/pagemodel.json';

describe('RelationshipIncomeComponent', () => {
  let component: RelationshipIncomeComponent;
  let fixture: ComponentFixture<RelationshipIncomeComponent>;
  let service: CustomerService;
  let appEntityComponent: AppEntityFilterComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AngularMaterialModule, AppEntityFilterModule],
      declarations: [RelationshipIncomeComponent, OrderByPipe]
    })
      .compileComponents();
  });

  beforeEach(() => {
    service = TestBed.inject(CustomerService);

    fixture = TestBed.createComponent(RelationshipIncomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call services', () => {
    spyOn(service, 'getHeaders');
    spyOn(service, 'getPageModel');

    component.ngOnInit();

    expect(service.getHeaders).toHaveBeenCalled();
    expect(service.getPageModel).toHaveBeenCalled();
  });

  it('should call services', () => {
    spyOn(service, 'getHeaders').and.callFake(() => of([{ header: 'income', value: 123 }]));
    spyOn(service, 'getPageModel').and.callThrough();

    component.ngOnInit();

    component.$relationshipObject.subscribe(x => {
      expect(x).toEqual(PageModelData);
    })

    component.$summary.subscribe(x => {
      expect(x).toEqual([{ header: 'income', value: 123 }]);
    })

  });
});
