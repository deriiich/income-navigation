import { SelectionModel } from '@angular/cdk/collections';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { _MatTableDataSource } from '@angular/material/table';
import { map, Observable, of, switchMap } from 'rxjs';
import { CustomerService } from '../services/customer.service';

@Component({
  selector: 'app-relationship-income',
  templateUrl: './relationship-income.component.html',
  styleUrls: ['./relationship-income.component.scss']
})

export class RelationshipIncomeComponent implements OnInit {
  displayedColumns: string[] = ['select', 'category', '2020_revenue', '2020_roe', 'ytd_revenue', 'ytd_roe', '2021_revenue', '2021_roe', '2022_revenue', '2022_roe', 'lifetime_revenue', 'lifetime_roe'];

  selection = new SelectionModel<any>(true, []);

  $relationshipObject: Observable<any> = of();
  $summary: Observable<any> = of();

  constructor(private service: CustomerService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.$relationshipObject = this.service.getPageModel();
    this.$summary = this.service.getHeaders();
  }

  displayDialog(event: any) {
    this.dialog.open(ChangeUserDialog, { data: event });
  }
}

export interface DialogData {
  id: number;
  name: string;
}

@Component({
  selector: 'change-user-dialog',
  templateUrl: 'change-user-dialog.html'
})

export class ChangeUserDialog {
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) { }
}