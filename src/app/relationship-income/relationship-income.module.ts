import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RelationshipIncomeRoutingModule } from './relationship-income-routing.module';
import { ChangeUserDialog, RelationshipIncomeComponent } from './relationship-income.component';
import { AngularMaterialModule } from '../angular-material.module';
import { AppEntityFilterModule } from '../app-entity-filter/app-entity-filter.module';
import { OrderByPipe } from '../utils/order-by.pipe';

@NgModule({
  declarations: [RelationshipIncomeComponent, ChangeUserDialog, OrderByPipe],
  imports: [
    CommonModule,
    RelationshipIncomeRoutingModule,
    AngularMaterialModule,
    AppEntityFilterModule,
  ],
})
export class RelationshipIncomeModule { }
