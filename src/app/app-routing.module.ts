import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const routes: Routes = [

  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'relationship-income'
  },

  {
    path: 'relationship-income',
    loadChildren: () => import('./relationship-income/relationship-income.module').then(m => m.RelationshipIncomeModule),
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
  },

  {
    path: '**',
    loadChildren: () => import('./not-ready/not-ready.module').then(m => m.NotReadyModule),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
