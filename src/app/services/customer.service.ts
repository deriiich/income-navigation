import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { PageModel } from '../models/page-model';
import PageModelJson from '../../assets/pagemodel.json'

@Injectable({
  providedIn: 'root'
})

export class CustomerService {

  mockData = PageModelJson;

  constructor() { }

  getHeaders(): Observable<PageModel[]> {

    const object: PageModel[] = [
      {
        header: 'Total Revenue',
        value: 400000,
      },
      {
        header: 'Credit',
        value: 200000,
      },
      {
        header: 'Deposits',
        value: 120000,
      },
      {
        header: 'Services',
        value: 80000,
      },
    ];

    return of(object);
  }

  getPageModel(): Observable<{ relationship: any; summary: any }> {
    return of(this.mockData);
  }
}
