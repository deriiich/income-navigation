import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { CustomerService } from './customer.service';
import PageModelData from '../../assets/pagemodel.json';

describe('CustomerService', () => {
  let service: CustomerService;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CustomerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('Get Headers()', () => {
    it('should return an array with object properties {header: string, value: number}', () => {
      spyOn(service, 'getHeaders').and.callFake(() => of([{ header: 'income', value: 123 }]));

      service.getHeaders().subscribe(x => {
        expect(x).toEqual([{ header: 'income', value: 123 }])
      });
    })
  })

  describe('Get PageModel()', () => {
    it('should return the mockData', () => {
      spyOn(service, 'getPageModel').and.callThrough();
      service.getPageModel().subscribe(x => {
        expect(x).toBe(PageModelData)
      });
    })
  })
});
